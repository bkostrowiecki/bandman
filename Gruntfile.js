module.exports = function(grunt) {

  grunt.initConfig({
    nggettext_extract: {
      pot: {
        files: {
          'src/front/lang/template.pot': ['src/front/templates/**/*.html']
        }
      },
    },
    jsdoc: {
      dev: {
        src: ['src/front/js/**/*.js'],
        dest: 'docs'
      }
    },
    wiredep: {
      dev: {
        fileTypes: {
          html: {
            replace: {
              js: '<script src="/front/{{filePath}}"></script>',
              css: '<link rel="stylesheet" type="text/css" href="/front/{{filePath}}">'
            }
          }
        },
          directory: 'src/front/vendor/',
        src: [
          'src/front/index.html'
        ],
      }
    },
    includeSource: {
      dev: {
        options: {
          templates: {
            html: {
              js: '<script src="{filePath}"></script>',
              css: '<link rel="stylesheet" type="text/css" href="/front/{filePath}">'
            }
          }
        },
        files: {
          'src/front/index.html': 'src/front/index.html'
        }
      }
    },
    clean: {
      dist: {
        options: {
          force: true,
          noWrite: true,
        },
        src: [
          'dist/**/*'
        ]
      },
    },
    copy: {
      dist: {
        files: [{
            src: [
              '**/*.*',
              '!vendor/**/*.*'
            ],
            cwd: 'src/front/',
            dest: 'dist/front/',
            expand: true
          },
          {
            src: [
              '**/*.*',
              '!composer.json',
              '!composer.lock',
              '!development.mode'
            ],
            cwd: 'src/api',
            dest: 'dist/api/',
            expand: true
          },
          {
            src: [
              'src/.htaccess'
            ],
            dest: 'dist/.htaccess',
            expand: false,
            dot: true
          },
          {
            src: [
              'src/api/.htaccess'
            ],
            dest: 'dist/api/.htaccess',
            expand: false,
            dot: true
          }]
      }
    },
    replace: {
      dist: {
        force: true,
        src: [ 'dist/front/index.html' ],
        overwrite: true,
        replacements: [{
            from: /<!-- bower:js -->/g,
            to: '<!-- bower:js '
          },
          {
            from: /<!-- endbower -->/g,
            to: ' endbower -->'
          },
          {
            from: /<!-- bower:css -->/g,
            to: '<!-- bower:css '
          },
          {
            from: /<!-- endbower -->/g,
            to: ' endbower -->'
          },
          {
            from: /<!-- bundle:css -->/g,
            to: '<link rel="stylesheet" type="text/css" href="/front/css/vendor.min.css">'
          },
          {
            from: /<!-- bundle:js -->/g,
            to: '<script src="/front/js/vendor.min.js"></script>'
          }
        ]
      }
    },
    cssmin: {
      dist: {
        files: {
          'dist/front/css/style.min.css': ['dist/front/css/**/*.css']
        }
      }
    },
    uglify: {
      options: {
        mangle: false
      },
      dist: {
        files: {
          'dist/front/js/app.min.js': ['src/front/js/**/*.js']
        }
      }
    },
    bower_concat: {
      dist: {
        dest: 'dist/front/js/vendor.min.js',
        cssDest: 'dist/front/css/vendor.min.css',
        mainFiles: {
          'bootstrap': 'dist/css/bootstrap.css'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-wiredep');
  grunt.loadNpmTasks('grunt-include-source');
  grunt.loadNpmTasks('grunt-jsdoc');

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-text-replace');
  grunt.loadNpmTasks('grunt-bower-concat');

  grunt.registerTask('default', []);

  grunt.registerTask('dev_docs', ['jsdoc:dev']);

  // Rebuild index.html based on dependiecies from 'bower.json' and include every '*.js' file from 'js' directory
  grunt.registerTask('dev_rebuild_deps', ['includeSource:dev', 'wiredep:dev']);

  grunt.registerTask('build', ['clean:dist', 'copy:dist', 'cssmin:dist', 'uglify:dist', 'bower_concat:dist', 'replace:dist']);

  // Create .pot file from the source code
  grunt.registerTask('prepareTranslation', 'nggettext_extract');
};