<?php

use \Slim\Slim as Slim;
use \Slim\Log as Log;

$app = Slim::getInstance();

$app->configureMode('development', function () use ($app) {
  $app->config(array(
    'debug' => true,
    'log.level' => Log::DEBUG,
    'log.enabled' => true,
    'cookies.encrypt' => false,
    'cookies.lifetime' => '60 minutes',
    'cookies.path' => '/',
    'cookies.secure' => false,
    'cookies.httponly' => true,
    'http.version' => '1.1'
  ));
});
