<?php 

require_once 'vendor/autoload.php';

use \Slim\Slim as Slim;
use \Slim\Middleware\SessionCookie as SessionCookie;

$app = new Slim(array(
  'mode' => (file_exists('development.mode') ? 'development' : 'production')
));

$app->add(new SessionCookie(array(
    'expires' => '20 minutes',
    'path' => '/',
    'domain' => null,
    'secure' => false,
    'httponly' => false,
    'name' => 'bandman',
    'secret' => 'SOME_SECKRET_STUFF_LOL',
    'cipher' => MCRYPT_RIJNDAEL_256,
    'cipher_mode' => MCRYPT_MODE_CBC
)));

require_once 'config/development.php';
require_once 'config/production.php';

require_once 'middleware/response.php';
require_once 'middleware/auth.php';

$app->add(new ApiResponseMiddleware());

require_once 'router.php';

$app->run();