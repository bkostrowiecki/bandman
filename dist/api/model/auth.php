<?php

use \Slim\Slim as Slim;

$app = Slim::getInstance();

$app->get("/token", function () use ($app) {
  /* Here generate and return JWT to the client. */
  var_dump($app->jwt);

  $app->response->setBody(json_encode(array("test" => "Test")));
});