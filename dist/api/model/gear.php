<?php

use \Slim\Slim as Slim;

$app = Slim::getInstance();

$app->get('/', function () {
    echo "Hello world";
});
  
$app->get('/login/:password', function ($password) use ($app) {
  $app->setCookie('passw0rd', $password);
});

$app->get('/hello/:name', function ($name) use ($app) {
  if (authenticate()) {
    $app->response->setBody("Hello, ");
  }
});