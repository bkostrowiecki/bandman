<?php

use \Slim\Slim as Slim;

$app = Slim::getInstance();

$app->configureMode('production', function () use ($app) {
  $app->config(array(
    'debug' => true,
    'log.level' => Log::DEBUG,
    'log.enabled' => true,
    'cookies.encrypt' => false,
    'cookies.lifetime' => '20 minutes',
    'cookies.path' => '/',
    'cookies.secure' => false,
    'cookies.httponly' => true,
    'http.version' => '1.1'
  ));
});
