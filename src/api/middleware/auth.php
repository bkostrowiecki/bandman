<?php

use \Slim\Slim as Slim;
use \Slim\Middleware as Middleware;

$app = Slim::getInstance();

$app->add(new \Slim\Middleware\JwtAuthentication([
    "secure" => false,
    "secret" => "supersecretkeyyoushouldnotcommittogithub",
    "rules" => [
        new Middleware\JwtAuthentication\RequestPathRule([
            "path" => "/",
            "passthrough" => ['/token', '/ping']
        ]),
        new Middleware\JwtAuthentication\RequestMethodRule([
            "passthrough" => ["OPTIONS"]
        ])
    ]
]));