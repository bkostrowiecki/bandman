<?php

use \Slim\Slim as Slim;
use \Slim\Middleware as Middleware;

$app = Slim::getInstance();

class ApiResponseMiddleware extends Middleware
{
  public function call()
  {
    // Get reference to application
    $app = $this->app;

    $res = $app->response;
    $res->headers->set('Content-Type', 'application/json; charset=utf-8');
    $res->headers->set('pageEncoding', 'utf-8');
    
    // Run inner middleware and application
    $this->next->call();
  }
}